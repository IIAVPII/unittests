using System;
using System.Threading.Tasks;
using HrDepartment.Application.Interfaces;
using HrDepartment.Application.Services;
using HrDepartment.Domain.Entities;
using HrDepartment.Domain.Enums;
using Moq;
using NUnit.Framework;

namespace HrDepartment.Tests
{
    public class JobSeekerRateServiceTests
    {
        private JobSeekerRateService _jobSeekerRateService;
        private ISanctionService _sanctionService;

        [SetUp]
        public void Setup()
        {
            _sanctionService = MockedSanctionService;
            _jobSeekerRateService = new JobSeekerRateService(_sanctionService);
        }


        [TestCase("Random", "Random", "Random", null, 0, 0, 0)]
        [TestCase("Random", "Random", "Random", null, 1, 1, 2)]
        [TestCase("Random", "Random", "Random", null, 2, 2, 4)]
        [TestCase("Random", "Random", "Random", null, 3, 4, 6)]
        [TestCase("Random", "Random", "Random", null, 3, 4, 8)]
        [TestCase("Random", "Random", "Random", null, 3, 4, 12)]
        public async Task CalculateJobSeekerRatingAsyncTestSuccess(string lastName, string firstName, string middleName,
            string birthDateStr, int eduLevel, int badHabit, double experience)
        {

            var js = new JobSeeker
            {
                LastName = lastName,
                FirstName = firstName,
                MiddleName = middleName,
                BirthDate = Convert.ToDateTime(birthDateStr),
                Education = (EducationLevel)eduLevel,
                BadHabits = (BadHabits)badHabit,
                Experience = experience
            };

            var result = await _jobSeekerRateService.CalculateJobSeekerRatingAsync(js);

            Assert.IsNotNull(result);
            Assert.IsTrue(result != 0);

            var tmpRes = 0;

            switch ((EducationLevel)eduLevel)
            {
                case EducationLevel.None:
                    tmpRes += 0;
                    break;
                case EducationLevel.School:
                    tmpRes += 5;
                    break;
                case EducationLevel.College:
                    tmpRes += 15;
                    break;
                case EducationLevel.University:
                    tmpRes += 35;
                    break;
                default:
                    Assert.Throws<ArgumentOutOfRangeException>(() => { });
                    break;
            }

            switch (experience)
            {
                case var e when experience < 1:
                    tmpRes += 5;
                    break;
                case var e when experience < 3 && experience >= 1:
                    tmpRes += 10;
                    break;
                case var e when experience < 5 && experience >= 3:
                    tmpRes += 15;
                    break;
                case var e when experience < 10 && experience >= 5:
                    tmpRes += 25;
                    break;
                case var e when experience >= 10:
                    tmpRes += 40;
                    break;
            }


            tmpRes += 10;

            switch ((BadHabits)badHabit)
            {
                case BadHabits.None:
                    tmpRes += 0;
                    break;
                case BadHabits.Smoking:
                    tmpRes -= 5;
                    break;
                case BadHabits.Alcoholism:
                    tmpRes -= 15;
                    break;
                case BadHabits.Drugs:
                    tmpRes -= 45;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(badHabit), badHabit, null);
            }

            Assert.IsTrue(result >= tmpRes);
            Assert.IsTrue(tmpRes >= 0);

            Assert.Pass($"from service: {result}, from tmp count: {tmpRes}");
        }

        [TestCase("Sanctioned", "Sanctioned", "Sanctioned", "2021-09-01")]
        public async Task CalculateJobSeekerRatingAsyncTestReturnsZero(string lastName, string firstName, string middleName, string birthDateStr)
        {
            var js = new JobSeeker
            {
                LastName = "Sanctioned",
                FirstName = "Sanctioned",
                MiddleName = "Sanctioned",
                BirthDate = Convert.ToDateTime(birthDateStr)
            };

            var result = await _jobSeekerRateService.CalculateJobSeekerRatingAsync(js);

            Assert.IsNotNull(result);
            Assert.IsTrue(result == 0);
            Assert.Pass();
        }

        private static ISanctionService MockedSanctionService
        {
            get
            {
                var mockService = new Mock<ISanctionService>();

                mockService.Setup(service =>
                        service.IsInSanctionsListAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<DateTime>()))
                    .Returns(FalseValueFunction);

                mockService.Setup(service =>
                        service.IsInSanctionsListAsync("Sanctioned", "Sanctioned", "Sanctioned", It.IsAny<DateTime>()))
                    .Returns(TrueValueFunction);


                return mockService.Object;
            }
        }

        private static async Task<bool> TrueValueFunction()
        {
            await Task.Yield();
            return true;
        }

        private static async Task<bool> FalseValueFunction()
        {
            await Task.Yield();
            return false;
        }
    }
}